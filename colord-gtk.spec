Name:           colord-gtk
Version:        0.3.1
Release:        1
Summary:        A GTK+ support library
License:        LGPLv2+
URL:            http://www.freedesktop.org/software/colord/
Source0:        http://www.freedesktop.org/software/colord/releases/%{name}-%{version}.tar.xz

BuildRequires:  gettext docbook-utils glib2-devel colord-devel intltool lcms2-devel 
BuildRequires:  gobject-introspection-devel vala gtk3-devel gtk-doc
BuildRequires:  meson gtk4-devel docbook5-style-xsl

%description
Colord is a system service that makes it easy to manage, install and generate color profiles
to accurately color manage input and output devices.
Colord-gtk is a GTK+ support library for colord.

%package devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains development files for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -Ddocs=true -Dgtk2=false -Dman=true -Dtests=false -Dvapi=true
%meson_build

%install
%meson_install

%delete_la_and_a
%ldconfig_scriptlets

%find_lang %{name}

%files -f %{name}.lang
%license AUTHORS COPYING
%{_bindir}/*
%{_libdir}/libcolord-gtk.so.*
%{_libdir}/libcolord-gtk4.so.*
%{_libdir}/girepository-1.0/ColordGtk-1.0.typelib

%files devel
%{_libdir}/libcolord-gtk.so
%{_libdir}/libcolord-gtk4.so
%{_libdir}/pkgconfig/colord-gtk.pc
%{_libdir}/pkgconfig/colord-gtk4.pc
%{_includedir}/colord-1/colord-gtk.h
%{_includedir}/colord-1/colord-gtk/*.h
%{_datadir}/gir-1.0/ColordGtk-1.0.gir
%{_datadir}/vala/vapi/colord-gtk.vapi
%{_datadir}/vala/vapi/colord-gtk.deps

%files help
%doc README NEWS
%doc %{_datadir}/gtk-doc/html/colord-gtk
%{_mandir}/man1/*.1.*

%changelog
* Fri May 17 2024 tenglei <tenglei@kylinos.cn> - 0.3.1-1
- Update package to version 0.3.1
- Fix the callback signature to fix a crash

* Wed Apr 20 2022 dillon chen <dillon.chen@gmail.com> - 0.3.0-1
- Update to 0.3.0

* Tue Nov 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.1.26-10
- Package init
